const jwt = require("jsonwebtoken");
const config = require("../config/auth.config.js");

// verifyToken = (req, res, next) => {
//   console.log(req.headers.authorization);
//   let token = req.headers.authorization;

//   if (!token) {
//     return res.status(403).send({ message: "No token provided" });
//   }

//   jwt.verify(token, config.secret, (err, decoded) => {
//     if (err) {
//       return res.status(401).send({
//         message: "Unauthorized",
//       });
//     }
//     req.userId = decoded.id;
//     console.log(req.userId);
//     next();
//   });
// };

verifyTokens = (req, res, next) => {
  console.log(req.headers.authorization);
  if (!req.headers.authorization) {
    res.status(401).send({ message: "Unauthorized" });
  } else {
    jwt.verify(req.headers.authorization, config.secret, (err, decoded) => {
      console.log(decoded);
      if (decoded) {
        req.user = decoded.data;
        next();
      } else {
        res.status(401).send({ message: "Unauthorized" });
      }
    });
  }
};

module.exports = verifyTokens;
