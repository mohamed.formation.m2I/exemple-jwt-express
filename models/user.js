module.exports = (sequelize, Datatypes) => {
  return sequelize.define(
    "User",
    {
      id: {
        type: Datatypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      username: {
        type: Datatypes.STRING,
        unique: { msg: "Username existe déjà" },
        allowNull: false,
      },
      email: {
        type: Datatypes.STRING,
        unique: { msg: "Email existe déjà" },
        allowNull: false,
      },
      password: {
        type: Datatypes.STRING,
        allowNull: false,
      },
    },
    {
      createdAt: false,
      updatedAt: false,
    }
  );
};
