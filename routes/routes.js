const express = require("express");
const router = express.Router();

const { register, login, profile } = require("../controllers/controllerUser");

const checkUpUsernameAndEmail = require("../middleware/verifyRegister");
const verifyTokens  = require("../middleware/auth.jwt");

router.post("/register", checkUpUsernameAndEmail, register);
router.post("/login", login);
router.get("/profile", verifyTokens, profile);

module.exports = router;
