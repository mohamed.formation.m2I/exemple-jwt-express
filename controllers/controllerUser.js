const express = require("express");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../config/sequelize");
const config = require("../config/auth.config");

exports.register = function (req, res, next) {
  try {
    let { username, email, password } = req.body;
    bcrypt.hash(password, 10).then((password) => {
      User.create({
        username: username,
        email: email,
        password: password,
      }).then((user) => {
        res.send({ message: "User register successfully", data: user });
      });
    });
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
};

exports.login = function (req, res, next) {
  try {
    let { username, password } = req.body;

    User.findOne({
      where: {
        username: username,
      },
    }).then((user) => {
      if (!user) {
        return res.status(400).send({ message: "User not found." });
      }

      var passwordIsValid = bcrypt.compareSync(password, user.password);

      if (!passwordIsValid) {
        return res
          .status(401)
          .send({ token: null, message: "Invalid Password" });
      }

      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 86400,
      });

      res.send({
        data: {
          id: user.id,
          username: user.username,
          email: user.email,
          token: token,
        },
        status: 200,
      });
    });
  } catch (error) {
    res.status(500).send({ error: error.message });
  }
};

exports.profile = function (req, res, next) {
  res.send({
    status: 1,
    data: { userName: "C'est moi", userWebsite: "https://abdallah.com" },
    message: "Successful",
  });
  next();
};
