const express = require("express");
const indexRouter = require("./routes/routes");
const cors = require("cors");
const sequelize = require("./config/sequelize");

const app = express();
app.use(cors());
app.use(express.json());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
  res.header("Access-Control-Allow-Headers", "*");
  next();
});

app.use("/api/user", indexRouter);

sequelize.sync();

app.listen(4000, () => {
  console.log("listening on port 4000");
});
