const { Sequelize, DataTypes } = require("sequelize");
const UserModel = require("../models/user");


const sequelize = new Sequelize("simpleangular", "root", "", {
  host: "localhost",
  dialect: "mariadb",
  dialectOptions: {
    timezone: "Etc/GMT-2",
  },
  logging: false,
});


const User = UserModel(sequelize, DataTypes);



module.exports = User;

